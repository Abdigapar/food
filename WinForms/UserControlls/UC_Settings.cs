﻿using System;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using WinForms.Forms;

namespace WinForms.UserControlls
{
    public partial class UC_Settings : UserControl
    {
        public UC_Settings()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            var language = comboBox1.Text;

            switch (language)
            {
                case "English":
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-EN");
                    break;
                case "Русский":
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("ru-RU");
                    break;
                case "Казахский":
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("ru-KZ");
                    break;
            }
        }
    }
}
