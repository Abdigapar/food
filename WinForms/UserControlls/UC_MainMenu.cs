﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinForms.Context;

namespace WinForms.UserControlls
{
    public partial class UC_MainMenu : UserControl
    {
        private readonly DefaultContext _context;
        Random rand = new Random();

        public UC_MainMenu()
        {
            InitializeComponent();
            _context = new DefaultContext();
            LoadStatistic();
        }

        private void LoadStatistic()
        {
            var next = rand.Next(13);
            var user = _context.Users.ToList();

            labelPower.Text = next.ToString();
            labelUser.Text = user.Count.ToString();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
