﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Microsoft.EntityFrameworkCore;
using WinForms.Context;
using WinForms.Models;

namespace WinForms.UserControlls
{
    public partial class UC_Menu : UserControl
    {
        private readonly DefaultContext _context;

        public UC_Menu()
        {
            InitializeComponent();
            LoadData();
            _context = new DefaultContext();
        }
        private void button2_Click_1(object sender, EventArgs e)
        {
            var cd = comboBox1.SelectedIndex + 1;
            var ca = comboBoxAge.SelectedIndex + 1;


            var res = _context.Diets.Include(c => c.Foods)
                .Where(c => c.Day == (DayOfTheWeekType)cd
                            && c.UserType == (UserType)ca)
                .Select(c => new
                {
                    foodTime = FoodTimes.GetNameById(c.TimeEat),
                    foodName = c.Food.Name,
                    protein = c.Food.Protein,
                    fat = c.Food.Fat,
                    Carbohydrate = c.Food.Сarbohydrate
                });

            dataGridView1.DataSource = res.ToList();

            // sum od protein
            var proteinSum = res.Sum(arg => arg.protein);
            labelProtein.Text = proteinSum.ToString(CultureInfo.InvariantCulture);

            //sum od calories
            var fatSum = res.Sum(c => c.fat);
            labelFat.Text = fatSum.ToString(CultureInfo.InvariantCulture);

            // sum od carb 
            var carbSum = res.Sum(c => c.Carbohydrate);
            labelCarb.Text = carbSum.ToString(CultureInfo.InvariantCulture);

            //sum od energy
            var energySum = res.Sum(c => 17.2* (c.protein + c.Carbohydrate) + (39.1*c.fat));
            labelEnergy.Text = energySum.ToString(CultureInfo.InvariantCulture);

        }
        public void LoadData()
        {
            foreach (DayOfTheWeekType week in Enum.GetValues(typeof(DayOfTheWeekType)))
            {
                comboBox1.Items.Add(Weeks.GetNameById(week));
            }

            foreach (UserType week in Enum.GetValues(typeof(UserType)))
            {
                comboBoxAge.Items.Add(UserTypes.GetNameById(week));
            }
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }
    }
}
