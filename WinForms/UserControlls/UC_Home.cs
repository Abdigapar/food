﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Microsoft.EntityFrameworkCore;
using WinForms.Context;
using WinForms.Forms.UC_Additionally;
using WinForms.Models;
using Menu = System.Windows.Forms.Menu;

namespace WinForms.UserControlls
{
    public partial class UC_Home : UserControl
    {
        private readonly DefaultContext _context;
        public static Menu Menu;
        private static DateTime _currentDate;

        public UC_Home()
        {
            InitializeComponent();
            LoadData();
            _currentDate = new DateTime();
            _context = new DefaultContext();

            timer1.Start();
            comboBoxUserType.SelectedIndexChanged += comboBoxUser_SelectedIndexChanged;
        }

        private void button2_Click(object sender, EventArgs e)
        {

            var cd = comboBoxDay.SelectedIndex + 1;
            var cut = comboBoxUserType.SelectedIndex + 1;
            var cu = comboBoxFoodTimeType.SelectedIndex + 1;


            var result = _context.Menus.Include(m => m.Food).Include(c=>c.Users)
                .Where(c => c.DayOfTheWeekType == (DayOfTheWeekType) cd
                            && c.UserType == (UserType) cut
                            && c.FoodTime == (FoodTimeType)cu)
                .Select(c => new
                 {
                     Day = Weeks.GetNameById(c.DayOfTheWeekType),
                     Category = UserTypes.GetNameById(c.UserType),
                     firstName = c.Users.FirstName,
                     lastName = c.Users.LastName,
                     age = c.Users.Age
                 });
            dataGridView1.DataSource = result.ToList();
        }

        public void LoadData()
        {
            foreach (DayOfTheWeekType week in Enum.GetValues(typeof(DayOfTheWeekType)))
            {
                comboBoxDay.Items.Add(Weeks.GetNameById(week));
            }

            foreach (UserType userType in Enum.GetValues(typeof(UserType)))
            {
                comboBoxUserType.Items.Add(UserTypes.GetNameById(userType));
            }

            foreach (FoodTimeType foodTime in Enum.GetValues(typeof(FoodTimeType)))
            {
                comboBoxFoodTimeType.Items.Add(FoodTimes.GetNameById(foodTime));
            }
        }

        private void comboBoxUser_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void UC_Home_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            AddNewVisitor visitor = new AddNewVisitor();
            visitor.ShowDialog();
        }
    }
}
