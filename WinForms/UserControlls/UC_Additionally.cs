﻿using System;
using System.Linq;
using System.Windows.Forms;
using WinForms.Context;
using WinForms.Forms.UC_Additionally;

namespace WinForms.UserControlls
{
    public partial class UC_Additionally : UserControl
    {
        private readonly DefaultContext _context;

        public UC_Additionally()
        {
            InitializeComponent();
            LoadDate();
            _context = new DefaultContext();
        }

        private void btnAddNewFoods_Click_1(object sender, EventArgs e)
        {
            AddNewFoods anf = new AddNewFoods();
            anf.ShowDialog();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            var anp = new AddNewMenu();
            anp.ShowDialog();
        }


        private void LoadDate()
        {
            comboBox1.Items.AddRange(new []{"Продукты", "Блюдо"});
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text == "Продукты")
            {
                dataGridView1.DataSource = _context.Products.ToList();
            }
            else if(comboBox1.Text == "Блюдо")
            {
                dataGridView1.DataSource = _context.Foods.ToList();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            AddNewProduct product = new AddNewProduct();
            product.ShowDialog();
        }
    }
}
