﻿using System.Collections.Generic;
using System.Linq;

namespace WinForms.Models
{
    public enum FoodType
    {
        Soups = 1,
        MainDishes = 2,
        Salad = 3,
        BakeryPastry = 4,
        Drinks = 5,
        FruitsVegetables = 6,
        MeatDairy = 7
    }

    public class FooTypeViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public static class FoodTypes
    {
        private static readonly List<FooTypeViewModel> FooTypeList = new List<FooTypeViewModel>()
        {
            new FooTypeViewModel{Id = 1, Name = "Первое блюдо"},
            new FooTypeViewModel{Id = 2, Name = "Второе бюдо"},
            new FooTypeViewModel{Id = 3, Name = "Салат"},
            new FooTypeViewModel{Id = 4, Name = "Хлеболуочные и кондитерские изделья"},
            new FooTypeViewModel{Id = 5, Name = "Напитки"},
            new FooTypeViewModel{Id = 6, Name = "Фрукты и овощи"},
            new FooTypeViewModel{Id = 7, Name = "Мясные и молочные"},
        };

        public static string GetNameById(FoodType? foodType)
        {
            var foodTypeName = FooTypeList
                .FirstOrDefault(c => c.Id == (int) foodType)?.Name;
            return foodTypeName;
        }
    }
}