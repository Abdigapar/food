﻿using System.Collections.Generic;

namespace WinForms.Models
{
    public class Menu
    {
        public int Id { get; set; }
        public FoodTimeType FoodTime { get; set; }
        public DayOfTheWeekType DayOfTheWeekType { get; set; }
        public UserType UserType { get; set; }  
        public User Users { get; set; }
        public Food Food { get; set; }
        public Diet Diet { get; set; }

    }
}