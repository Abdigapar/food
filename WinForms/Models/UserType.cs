﻿using System.Collections.Generic;
using System.Linq;

namespace WinForms.Models
{
    public enum UserType
    {
        Junior = 1,
        Senior = 2
    }

    public class UserTypeViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public static class UserTypes
    {
        private static readonly List<UserTypeViewModel> UserTypesList = new List<UserTypeViewModel>()
        {
            new UserTypeViewModel{Id = 1, Name = "Младшеклассник"},
            new UserTypeViewModel{Id = 2, Name = "Старшеклассник"},
        };

        public static string GetNameById(UserType userType)
        {
            var userName = UserTypesList
                .FirstOrDefault(c => c.Id == (int)userType)?.Name;
            return userName;
        }
    }
}