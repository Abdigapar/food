﻿using System.Collections.Generic;
using System.Linq;
namespace WinForms.Models
{
    public enum FoodTimeType
    {
        Breakfast = 1,
        Lunch = 2,
        Snack = 3,
        Dinner = 4
    }

    public class FoodTimeViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }


    public static class FoodTimes
    {
        private static readonly List<FoodTimeViewModel> FoodTypesList = new List<FoodTimeViewModel>()
        {
            new FoodTimeViewModel{Id = 1, Name = "Завтрак"},
            new FoodTimeViewModel{Id = 2, Name = "Обед"},
            new FoodTimeViewModel{Id = 3, Name = "Полдник"},
            new FoodTimeViewModel{Id = 4, Name = "Ужин"},
        };

        public static string GetNameById(FoodTimeType? foodTimeType)
        {
            var foodTypeName = FoodTypesList
                .FirstOrDefault(c => c.Id == (int)foodTimeType)?.Name;
            return foodTypeName;
        }
    }
}