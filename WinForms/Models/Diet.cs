﻿using System.Collections.Generic;

namespace WinForms.Models
{
    public class Diet
    {
        public Diet()
        {
            Foods = new List<Food>();
        }

        public int Id { get; set; }
        public FoodTimeType TimeEat { get; set; }
        public DayOfTheWeekType Day { get; set; }
        public UserType UserType { get; set; }
        public Food Food { get; set; }
        public ICollection<Food> Foods { get; set; }
    }
}