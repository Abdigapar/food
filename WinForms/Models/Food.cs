﻿using System.Collections.Generic;

namespace WinForms.Models
{
    public class Food
    {
        public Food()
        {
            FoodProducts = new List<FoodProduct>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public FoodType FoodType { get; set; }
        public double Protein { get; set; } // белки
        public double Fat { get; set; } // жиры
        public double Сarbohydrate { get; set; } // углеводы
        public double Mass { get; set; } // масса
        public ICollection<FoodProduct> FoodProducts { get; set; }

    }
}