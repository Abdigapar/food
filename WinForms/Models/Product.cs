﻿using System.Collections.Generic;

namespace WinForms.Models
{
    public class Product
    {
        public Product()
        {
            ProductFoods = new List<FoodProduct>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public double Protein { get; set; }
        public double Fat { get; set; }
        public double Сarbohydrate { get; set; }
        public double Mass { get; set; }
        public ICollection<FoodProduct> ProductFoods { get; set; }
    }

}