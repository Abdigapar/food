﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WinForms.Models
{
    public enum DayOfTheWeekType
    {
        Monday = 1,
        Tuesday = 2,
        Wednesday = 3,
        Thursday = 4,
        Friday = 5
    }

    public class WeekTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }


    public static class Weeks
    {
        private static readonly List<WeekTypeModel> WeekTypesList = new List<WeekTypeModel>()
        {
            new WeekTypeModel{Id = 1, Name = "Понедельник"},
            new WeekTypeModel{Id = 2, Name = "Вторник"},
            new WeekTypeModel{Id = 3, Name = "Среда"},
            new WeekTypeModel{Id = 4, Name = "Четверг"},
            new WeekTypeModel{Id = 5, Name = "Пятница"},
        };
        public static string GetNameById(DayOfTheWeekType weekType)
        {
            var weekName = WeekTypesList
                .FirstOrDefault(c => c.Id == (int) weekType)?.Name;
            return weekName;
        }

        public static int GetIdByName(DayOfTheWeekType weekType)
        {
            var weekName = WeekTypesList
                .FirstOrDefault(c => c.Id == (int)weekType)?.Id;
            return (int) weekName;
        }
    }
}