﻿using System;
using System.Windows.Forms;
using WinForms.Context;
using WinForms.Models;

namespace WinForms.Forms.UC_Additionally
{
    public partial class AddNewUser : Form
    {
        private readonly DefaultContext _context;

        public AddNewUser()
        {
            InitializeComponent();
            LoadUserType();
            _context = new DefaultContext();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var currentFirstName = textBoxFirstName.Text;
            var currentLastName = textBoxLastName.Text;
            var currentAge = textBoxAge.Text;
            var currentUserType = comboBoxAge.SelectedIndex + 1;

            var newUser = new User()
            {
                FirstName = currentFirstName,
                LastName = currentLastName,
                Age = Convert.ToInt32(currentAge),
                UserType = (UserType)currentUserType
            };

            _context.Add(newUser);
            _context.SaveChanges();

            MessageBox.Show("Новоый ученик");
        }


        private void LoadUserType()
        {
            foreach (UserType week in Enum.GetValues(typeof(UserType)))
            {
                comboBoxAge.Items.Add(UserTypes.GetNameById(week));
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
