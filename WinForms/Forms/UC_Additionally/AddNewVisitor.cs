﻿namespace WinForms.Forms.UC_Additionally
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Forms;
    using Context;
    using Models;

    public partial class AddNewVisitor : Form
    {
        private readonly DefaultContext _context;

        public AddNewVisitor()
        {
            InitializeComponent();
            LoadDate();
            _context = new DefaultContext();
            comboBoxUserType.SelectedIndexChanged += comboBoxName_SelectedIndexChanged;
            comboBoxFoodType.SelectedIndexChanged += comboBoxFood_SelectedIndexChanged;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var currentCategory = comboBoxUserType.SelectedIndex + 1;
            var currentName = comboBoxName.SelectedItem;
            var currentFoodTime = comboBoxFoodTimeType.SelectedIndex + 1;
            var currentDay = comboBoxDay.SelectedIndex + 1;
            var currentFood = comboBoxFood.SelectedItem;

            Models.Menu menu = new Models.Menu
            {
                DayOfTheWeekType = (DayOfTheWeekType) currentDay,
                FoodTime = (FoodTimeType) currentFoodTime,
                Users = (User) currentName,
                UserType = (UserType) currentCategory,
                Food = (Food) currentFood
            };

            _context.Add(menu);
            _context.SaveChanges();

           MessageBox.Show("Сохранено");
        }

        private void LoadDate()
        {
            foreach (FoodTimeType foodTimeType in Enum.GetValues(typeof(FoodTimeType)))
            {
                comboBoxFoodTimeType.Items.Add(FoodTimes.GetNameById(foodTimeType));
            }

            foreach (UserType week in Enum.GetValues(typeof(UserType)))
            {
                comboBoxUserType.Items.Add(UserTypes.GetNameById(week));
            }

            foreach (DayOfTheWeekType week in Enum.GetValues(typeof(DayOfTheWeekType)))
            {
                comboBoxDay.Items.Add(Weeks.GetNameById(week));
            }

            foreach (FoodType food in Enum.GetValues(typeof(FoodType)))
            {
                comboBoxFoodType.Items.Add(FoodTypes.GetNameById(food));
            }
        }

        private void LoadFood(List<Food> foods, string foodName)
        {
            comboBoxFood.DataSource = foods;
            comboBoxFood.DisplayMember = "Name";
        }


        private void LoadUser(List<User> users, string name)
        {
            comboBoxName.DataSource = users;
            comboBoxName.DisplayMember = name;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            AddNewUser newUser = new AddNewUser();
            newUser.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

      

        private void comboBoxName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
     
        private void comboBoxUserType_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectUserType = (UserType)(comboBoxUserType.SelectedIndex + 1);
            var selectUser = _context.Users.Where(c => c.UserType == selectUserType).ToList();

            switch (selectUserType)
            {
                case UserType.Junior:
                    LoadUser(selectUser, "FirstName");
                    break;
                case UserType.Senior:
                    LoadUser(selectUser, "FirstName");
                    break;
            }
        }

        private void comboBoxFoodType_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            var selectedFoodType = (FoodType)(comboBoxFoodType.SelectedIndex + 1);
            var selectedFood = _context.Foods.Where(c => c.FoodType == selectedFoodType).ToList();

            switch (selectedFoodType)
            {
                case FoodType.Soups:
                    LoadFood(selectedFood, "Name");
                    break;
                case FoodType.MainDishes:
                    LoadFood(selectedFood, "Name");
                    break;
                case FoodType.Salad:
                    LoadFood(selectedFood, "Name");
                    break;
                case FoodType.BakeryPastry:
                    LoadFood(selectedFood, "Name");
                    break;
                case FoodType.Drinks:
                    LoadFood(selectedFood, "Name");
                    break;
                case FoodType.FruitsVegetables:
                    LoadFood(selectedFood, "Name");
                    break;
                case FoodType.MeatDairy:
                    LoadFood(selectedFood, "Name");
                    break;
            }
        }

        private void AddNewVisitor_Load(object sender, EventArgs e)
        {

        }

        private void comboBoxFood_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
