﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using WinForms.Context;
using WinForms.Models;
using Menu = WinForms.Models.Menu;

namespace WinForms.Forms.UC_Additionally
{
    public partial class AddNewMenu : Form
    {
        private readonly DefaultContext _context;
        bool again = true;

        public AddNewMenu()
        {
            InitializeComponent();
            _context = new DefaultContext();
            LoadDate();
            comboBoxFoodType.SelectedIndexChanged += comboBox1_SelectedIndexChanged;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var message = "Добавить еще блюдо на текущий рацион?";
            var title = "Close Window";
            var boxButtons = MessageBoxButtons.YesNo;
            var result = MessageBox.Show(message, title, boxButtons);

            while (again)
            {
                var currentFood = (Food)comboBox1.SelectedItem;
                var currentFoodTime = comboBox2.SelectedIndex + 1;
                var currentDay = comboBox3.SelectedIndex + 1;
                var currentAge = comboBoxAge.SelectedIndex + 1; 

                var searchFood = _context.Foods.FirstOrDefault(c => c.Id == currentFood.Id);

                var newMenu = new Diet()
                {
                    Food = searchFood,
                    TimeEat = (FoodTimeType)currentFoodTime,
                    Day = (DayOfTheWeekType)currentDay,
                    UserType = (UserType) currentAge
                };

                if (result == DialogResult.Yes)
                {
                    again = false;
                    Close();
                    AddNewMenu newMenu2 = new AddNewMenu();
                    newMenu2.Show();
                    _context.Add(newMenu);
                }
                else
                {
                    _context.Add(newMenu);
                    MessageBox.Show("Новое блюдо");
                    again = false;
                    Close();
                }
            }
            _context.SaveChanges();
        }

        public void LoadFood(List<Food> foods, string foodName)
        {
            comboBox1.DataSource = foods;
            comboBox1.DisplayMember = "Name";
        }

        private void LoadDate()
        {
            foreach (UserType week in Enum.GetValues(typeof(UserType)))
            {
                comboBoxAge.Items.Add(UserTypes.GetNameById(week));
            }

            foreach (DayOfTheWeekType week in Enum.GetValues(typeof(DayOfTheWeekType)))
            {
                comboBox3.Items.Add(Weeks.GetNameById(week));
            }

            foreach (FoodTimeType foodTime in Enum.GetValues(typeof(FoodTimeType)))
            {
                comboBox2.Items.Add(FoodTimes.GetNameById(foodTime));
            }

            foreach (FoodType foodType in Enum.GetValues(typeof(FoodType)))
            {
                comboBoxFoodType.Items.Add(FoodTypes.GetNameById(foodType));
            }
        }


        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void comboBoxFoodType_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedFoodType = (FoodType)(comboBoxFoodType.SelectedIndex + 1);
            var selectedFood = _context.Foods.Where(c => c.FoodType == selectedFoodType).ToList();

            switch (selectedFoodType)
            {
                case FoodType.Soups:
                   LoadFood(selectedFood, "Name");
                    break;
                case FoodType.MainDishes:
                    LoadFood(selectedFood, "Name");
                    break;
                case FoodType.Salad:
                    LoadFood(selectedFood, "Name");
                    break;
                case FoodType.BakeryPastry:
                    LoadFood(selectedFood, "Name");
                    break;
                case FoodType.Drinks:
                    LoadFood(selectedFood, "Name");
                    break;
                case FoodType.FruitsVegetables:
                    LoadFood(selectedFood, "Name");
                    break;
                case FoodType.MeatDairy:
                    LoadFood(selectedFood, "Name");
                    break;
            }
        }

        private void comboBoxFoodType_Click(object sender, EventArgs e)
        {

        }
    }
}
