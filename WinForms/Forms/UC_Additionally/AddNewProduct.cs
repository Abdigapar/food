﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinForms.Context;
using WinForms.Models;

namespace WinForms.Forms.UC_Additionally
{
    public partial class AddNewProduct : Form
    {
        private readonly DefaultContext _context;

        public AddNewProduct()
        {
            InitializeComponent();
            _context = new DefaultContext();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var newProduct = new Product()
            {
                Name = textBoxProductName.Text,
                Fat = Convert.ToDouble(textBox8.Text),
                Сarbohydrate = Convert.ToDouble(textBox6.Text),
                Mass = Convert.ToDouble(textBox2.Text),
                Protein = Convert.ToDouble(textBox5.Text)
            };

            _context.Add(newProduct);
            _context.SaveChanges();
            MessageBox.Show("Сохранено");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
