﻿using System;
using System.Windows.Forms;
using WinForms.Context;
using WinForms.Models;

namespace WinForms.Forms.UC_Additionally
{
    public partial class AddNewFoods : Form
    {
        private readonly DefaultContext _context;

        public AddNewFoods()
        {
            InitializeComponent();
            _context = new DefaultContext();
            LoadFoodType();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var ft = comboBoxFoodType.SelectedIndex + 1;

            var newFood = new Food()
            {
                Name = textBox3.Text,
                FoodType = (FoodType)ft,
                Fat = Convert.ToDouble(textBox8.Text),
                Сarbohydrate = Convert.ToDouble(textBox6.Text),
                Mass = Convert.ToDouble(textBox2.Text),
                Protein = Convert.ToDouble(textBox5.Text)
            };

            _context.Add(newFood);
            _context.SaveChanges();
            MessageBox.Show("Вы успешно добавили Блюдо");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void LoadFoodType()
        {
            foreach (FoodType foodType in Enum.GetValues(typeof(FoodType)))
            {
                comboBoxFoodType.Items.Add(FoodTypes.GetNameById(foodType));
            }
        }
    }
}
