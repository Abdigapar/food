﻿using System;
using System.Linq;
using System.Windows.Forms;
using WinForms.Context;
using WinForms.Forms;
using WinForms.Models;

namespace WinForms
{
    public partial class LoginPage : Form
    {
        private readonly DefaultContext _context;
        public static User LoggedUser;
        public static Dashboard Form1;

        public LoginPage()
        {
            Form1 = new Dashboard();
            _context = new DefaultContext();
            InitializeComponent();
        }

        private void Menu_Load(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var currentLogin = textBox1.Text;
            var currentPassword = textBox2.Text;

            LoggedUser = _context.Users.FirstOrDefault(c => c.Login == currentLogin
                                                                    && c.Password == currentPassword);
            if (LoggedUser == null)
            {
                var errorText = $"Error";
                MessageBox.Show(errorText);
            }
            else
            {
                Hide();
                Form1.ShowDialog();
                Close();
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {
            var currentLogin = textBox1.Text;
            var searchUser = _context.Users.FirstOrDefault(c => c.Login == currentLogin);
            if (searchUser != null)
            {
                ForgotPassword forgotPassword = new ForgotPassword();
                forgotPassword.ShowDialog();
            }
            else
            {
                MessageBox.Show("Пользователь с таким именем не найден");
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {
            AddNewEmployee add = new AddNewEmployee();
            add.ShowDialog();
        }

        private void gradientPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void gradientPanel1_Paint_1(object sender, PaintEventArgs e)
        {

        }
    }
}
