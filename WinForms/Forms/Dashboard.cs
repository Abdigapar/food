﻿using System;
using System.Windows.Forms;
using WinForms.UserControlls;
using DateTime = System.DateTime;

namespace WinForms.Forms
{
    public partial class Dashboard : Form
    {
        public Dashboard()
        {
            InitializeComponent();
            timerTime.Start();

            UC_Home home = new UC_Home();
            AddControlsToPanel(home);
        }

        private void panel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            labelTimer.Text = dt.ToString("HH:MM:ss");
        }

        private void MoveSidePanel(Control btn)
        {
            panelSide.Top = btn.Top;
            panelSide.Height = btn.Height;
        }

        private void AddControlsToPanel(Control c)
        {
            c.Dock = DockStyle.Fill;
            PanelControlls.Controls.Clear();
            PanelControlls.Controls.Add(c);
        }
        private void btnHome_Click(object sender, EventArgs e)
        {
            MoveSidePanel(btnHome);
            UC_Home home = new UC_Home();
            AddControlsToPanel(home);
        }


        private void button1_Click(object sender, EventArgs e)
        {
            MoveSidePanel(buttonMainMenu);
            UC_MainMenu menu = new UC_MainMenu();
            AddControlsToPanel(menu);
        }

        private void btnMenu_Click(object sender, EventArgs e)
        {
            MoveSidePanel(btnMenu);
            UC_Menu menu = new UC_Menu();
            AddControlsToPanel(menu);
        }
        private void btnSettings_Click(object sender, EventArgs e)
        {
     
        }
        private void button3_Click(object sender, EventArgs e)
        {
            MoveSidePanel(btnAdditionally);
            UserControlls.UC_Additionally additionally = new UserControlls.UC_Additionally();
            AddControlsToPanel(additionally);
        }
        private void Dashboard_Load(object sender, EventArgs e)
        {
            label4.Text = LoginPage.LoggedUser.Login;
        }


        private void panelLeft_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {
        }

        private void labelTimer_Click(object sender, EventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox2_Click_1(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://myaccount.google.com/intro");
        }

        private void Reference_Inst(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.instagram.com");
        }

        private void Reference_FaceBook(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://web.facebook.com");
        }

        private void Reference_Skype(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.skype.com/ru");
        }

        private void Reference_WhatsApp(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://web.whatsapp.com");
        }

        private void PanelControlls_Paint(object sender, PaintEventArgs e)
        {

        }

    }
}

