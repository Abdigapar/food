﻿using System;
using System.Linq;
using System.Windows.Forms;
using WinForms.Context;
using WinForms.Models;

namespace WinForms.Forms
{
    public partial class AddNewEmployee : Form
    {
        private readonly DefaultContext _context;
        public AddNewEmployee()
        {
            InitializeComponent();
            _context = new DefaultContext();
        }
        
        private void button1_Click_1(object sender, EventArgs e)
        {
            var login = textBox1.Text.Trim();
            var password = textBox2.Text;

            var newUserLogin = _context.Users.FirstOrDefault(c => c.Login == login);

            if (newUserLogin != null)
            {
                MessageBox.Show("Пользователь с таким именем уже существует");
                return;
            }

            var newUser = new User()
            {
                Login = login,
                Password = password
            };
            _context.Add(newUser);
            _context.SaveChanges();
            MessageBox.Show("Вы успешно создали аккаунт");
            Close();
        }
    }
}
