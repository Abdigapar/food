﻿using System;
using System.Linq;
using System.Windows.Forms;
using WinForms.Context;
using WinForms.Models;

namespace WinForms.Forms
{
    public partial class ForgotPassword : Form
    {
        private readonly DefaultContext _context;
        public static User LoggedUser;
        public ForgotPassword()
        {
            InitializeComponent();
            _context = new DefaultContext();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            var currentPassword = textBox1.Text;
            LoggedUser = _context.Users.FirstOrDefault(c => c.Password == currentPassword);
            if (LoggedUser != null)
            {
                var newPassword = textBox2.Text;
                LoggedUser.Password = newPassword;
                _context.Update(LoggedUser);
                _context.SaveChanges();
                Close();
            }
            else
            {
                MessageBox.Show("введите старый пароль правильно");
            }
        }
    }
}
