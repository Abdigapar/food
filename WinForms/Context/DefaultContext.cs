﻿using Microsoft.EntityFrameworkCore;
using WinForms.Configs;
using WinForms.Models;

namespace WinForms.Context
{
    public class DefaultContext : DbContext
    {
        public DbSet<Food> Foods { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Menu> Menus { get; set; }
        public DbSet<Diet> Diets { get; set; }

        public DefaultContext()
        {
            Database.EnsureCreated();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FoodProduct>()
                .HasKey(t => new { t.FoodId, t.ProductId });

            modelBuilder.Entity<FoodProduct>()
                .HasOne(sc => sc.Food)
                .WithMany(s => s.FoodProducts)
                .HasForeignKey(sc => sc.FoodId);

            modelBuilder.Entity<FoodProduct>()
                .HasOne(sc => sc.Product)
                .WithMany(c => c.ProductFoods)
                .HasForeignKey(sc => sc.ProductId);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(AppConfig.ConnectionString);
        }
    }
}